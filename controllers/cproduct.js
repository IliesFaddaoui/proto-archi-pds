var Product = require("../schema/productSchema.js");

function createProduct(req, res) {
    var id = req.body.id;
    var name = req.body.name;
    var price = req.body.price;
    var store = req.body.store
    if(!id || !price || !store){
        return res.status(400).json({
            text: "Body incomplete"
        });
    }
        var product = {
            id,
            name,
            price,
            store
        };
        try {
            const findProduct = Product.findOne({id});
            if (findProduct){
                return res.status(400).json({
                    text: "id already used by another product"
                });
            }
        } catch(error) {
            res.status(500).json({
                error
            });
        }
        try{
            var productData = new Product(product);
            var productObject = productData.save();
            return res.status(200).json({
                text: "Succès"
            });
        } catch(error) {
            res.status(500).json({
                error
            });
        }

}

function findProductById(req, res) {
    var id = req.body.id;
    if(!id){
        res.status(400).json({
            text: "id is missing on body"
        });
    }
    try {
        Product.findById(id, function (err, product) {
            if (err) {
                return res.status(500).send(err)
            }
            return res.status(200).send(product);
        });
    } catch(error) {
        res.status(500).json({
            error
        });
    }
}

function deleteProductById(req, res) {
    var id = req.body.id;
    if(!id){
        res.status(400).json({
            text: "id is missing on body"
        });
    }
    try {
        Product.findByIdAndDelete(id, (err, product) => {
            if (err) {
                return res.status(500).send(err);
            }
            const response = {
                message: "product deleted",
                id: product._id
            };
            return res.status(200).send(response);
        });

    } catch(error) {
        res.status(500).json({
            error
        });
    }
}

function updateProductById(req, res) {

}