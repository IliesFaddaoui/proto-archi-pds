const mongoose = require('mongoose');

var productSchema = mongoose.Schema({
  id: {
    type : String,
    trim: true,
    unique: true,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  price:{
    type: Number,
    required: true
  },
  store:{
    type:String,
    required: true
  }
})

module.exports = mongoose.model('Product', productSchema);
