var express = require("express"),
  app = express(),
  port = process.env.PORT || 3000;
var fs = require("fs");
var mongoose = require('mongoose');
var bodyParser = require("body-parser");
var cproduct = require('./controllers/cproduct.js')
app.listen(port);
console.log('App run on port '+ port);

app.get('/getProductById', (req, res, next) => {
    cproduct.findProductById;
});

app.post('/postProduct', (req, res, next) => {
    cproduct.createProduct;
})
app.delete('/deleteProduct', (req, res, next) => {
    cproduct.deleteProductById;
})


mongoose.connect('mongodb://localhost/db').then(() => {
    console.log('Connected to mongoDB')
}).catch(e => {
    console.log('Error while DB connecting');
    console.log(e);
});
