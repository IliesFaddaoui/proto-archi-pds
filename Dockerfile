FROM node:9-alpine

WORKDIR '/var/www/app'

LABEL vendor="sc-project"
LABEL commidid="1"
LABEL buildAt="oui"
LABEL releaseVersion="v1.0.0-RELEASE"
LABEL snapshot="v1.0.0-SNAPSHOT"
